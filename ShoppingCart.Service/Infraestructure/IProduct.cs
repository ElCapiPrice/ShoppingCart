﻿using ShoppingCart.DataAccess.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShoppingCart.Service.Infraestructure
{
    public interface IProduct
    {
        List<Product> GetAllProducts();
        Product GetProductById(int Id);
        void InsertProduct(Product product);
        void UpdateProduct(Product product);
        void DeleteProduct(Product product);
        void Save();
    }
}
